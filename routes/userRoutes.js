// Setting up dependencies/router , importing route and auth files
const express = require('express');
const router = express.Router();
const auth = require('../auth');
const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order');
const userController = require('../controllers/userController');

// Route for checking duplicate emails
router.post("/checkDuplicateEmail", (req, res) => {
	userController.checkDuplicateEmail(req.body).then(result => res.send(result));
});

// Route for checking duplicate mobile numbers
router.post("/checkMobileNumber", (req, res) => {
	userController.checkMobileNumber(req.body).then(result => res.send(result));
});

// Passcode router
// router.post("/passcode", auth.verify, auth.admin, (req, res) => {
// 	userController.passcodeSave(req.body).then(result => res.send(result));
// });
/*
	{
	    "passcode": "DoNotGiveThisToAnyone199xx"
	}
*/

// Route for user registration
router.post("/registerUser", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
});

// Route for user logging in
router.post("/loginUser", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});

//Route for admin logging in
router.post("/loginAdmin", (req, res) => {
	userController.loginAdmin(req.body).then(result => res.send(result));
})

// Route for getting user details
router.get("/getProfile", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile(userData.id).then(result => res.send(result));
});

//Route for updating user's information
router.put("/updateProfile", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.updateProfile(userData.id,  req.body).then(result => res.send(result));
});

// Route for updating user's shipping address
router.put("/updateShippingAddress", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.updateShippingAddress(userData.id, req.body).then(result => res.send(result));
});

// Route for changing user's password
router.put("/changePass", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.changePass(userData.id, req.body).then(result => res.send(result))
});


// Route for user's deactivation
router.delete("/:userId/deactivate", auth.verify, (req, res) => {
	userController.deactivate(req.params).then(result => res.send(result));
});

// Route for getting all the user's details (Admin only)
router.get("/getAllProfile", (req, res) => {
	userController.getAllProfile().then(result => res.send(result));
});

// Setting user into admin
router.put("/:userId", auth.verify, auth.admin, (req, res) => {
	userController.updateToAdmin(req.params).then(result => res.send(result));
});	

// Setting admin into non-admin user
router.put("/:userId/nonAdmin", auth.verify, auth.admin, (req, res) => {
	userController.updateToNonAdmin(req.params).then(result => res.send(result));
});	

// Route for buying a product
router.post("/buy", auth.verify, (req, res) => {
	// Findig the product through productId then accessing its properties to be stored into a new order information whenever the user request a post/buy
	Product.findOne({_id: req.body.productId}).then(result => {
		const orderData = {
		userId:	auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		productName: result.productName,
		quantity: req.body.quantity,
		paymentMethod: req.body.paymentMethod,
		price: result.price, 
		subtotal: result.price * req.body.quantity
	}
	userController.buy(orderData).then(result => res.send(result))
	});
});

// Route for getting user the orders
router.get("/getOrder", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getOrder(userData.id).then(result => res.send(result));
});

// Route for for users to rate products
router.put("/:userOrderId/:productOrderId/rateProduct", auth.verify, (req, res) => {
	userController.rateProduct(req.params, req.body).then(result => res.send(result));
})

// Fucntion to retrieve all of the user's orders (Admin only)
router.get("/getAllOrder", auth.verify, auth.admin, (req, res) => {
	userController.getAllOrder().then(result => res.send(result));
});





// Making router exportable
module.exports = router