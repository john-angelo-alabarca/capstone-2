// Importing data models
const Product = require('../models/product');

// Function to get all available/active products
module.exports.getAll = () => {
	return Product.find().then(result => {
		return result;
    })
}

// Function to get a specific product
module.exports.getSpecificProduct = (params) => {
	// Finding the product and hiding the addedOn, id and isAvailable to display
	return Product.findById(params.productId).then(result => {
		return result;
	});
}

// Function to add a new product
module.exports.addProduct = (body) => {
	// Creating a new product to be saved
	let newProduct = new Product({
		productName: body.productName,
		stock: body.stock,
		category: body.category,
		description: body.description,
		size: body.size,
		price: body.price
	});
	// saving the new product into Product db
	return newProduct.save().then((product, error) => {
		if(error){
			return false; //save unsuccessful
		}else{
			return true; //save successful
		}
	});
}

// Function to update a product
module.exports.updateProduct = (params, body) => {
	// Creating new updated product
	let updatedProduct = {
		productName: body.productName,
		stock: body.stock,
		category: body.category,
		description: body.description,
		size: body.size,
		price: body.price
	}
	// Finding the product and updating the product in db
	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	});
}

// Function to delete a product (Archive)
module.exports.archiveProduct = (params, body) => {
	// Making the product availability to false
	let archivedProduct
	
	if(body.isAvailable === true){
		archivedProduct = {
			isAvailable: false
		}
	}else{
		archivedProduct = {
			isAvailable: true
		}
	}
	// finding and saving in into db
	return Product.findByIdAndUpdate(params.productId, archivedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	});
}

// Function to restore a product (unarchive)
module.exports.unArchiveProduct = (params) => {
	// Making the product availability to true
	let archivedProduct = {
		isAvailable: true
	}
	// finding and saving in into db
	return Product.findByIdAndUpdate(params.productId, archivedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	});
}