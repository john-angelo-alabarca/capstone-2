// Setting up the installed dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Importing routes
const productRoutes = require('./routes/productRoutes');
const userRoutes = require('./routes/userRoutes');


// Setting up the server
const app = express();
const port = 5000;

// const corsOptions = {
// 	origin: ['http://localhost:3000', 'https://secure-forest-68384.herokuapp.com'],
// 	optionsSuccessStatus: 200
// }

// Tells the server that cors is being used by your server
app.use(cors())

// 
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Prefixes to products, users, and orders routes

// app.use("/products", productRoutes);
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// Setting up the connection to MongoDB
mongoose.connect("mongodb+srv://admin:admin123@cluster0.l3txy.mongodb.net/b110_ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Mongoose connection
mongoose.connection.once("open", () => {
	console.log("Now connected to MongoDB Atlas.")
});

// Connecting to host/local port
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
});