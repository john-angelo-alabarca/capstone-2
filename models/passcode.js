// Setting up mongoose dependency
const mongoose = require('mongoose');

// Making a passcode schema (blueprint/model)
const passcodeSchema = new mongoose.Schema({
	isAdmin: {
		type: Boolean,
		default: true
	},
	passcode: {
		type: String,
		default: ""
	}
	
});

// Making product schema exportable
module.exports = mongoose.model("Passcode", passcodeSchema);