// Setting up mongoose dependency
const mongoose = require('mongoose');

// Making ratings schema (blueprint/model)
const ratingSchema = new mongoose.Schema({
	// productId: {
	// 	type: String,
	// 	required: [true, "Product Id is required"]
	// },
	// productName: {
	// 	type: String,
	// 	required: [true, "Product name is required"]
	// },
	// totalRatings: {
	// 	type: String,
	// 	default: 0
	// },
	userFeedback: [
		{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			userName: {
				type: String,
				required: [true, "User Id is required"]
			},
			userRating: {
				type: Number,
				required: [true, "User rating is required"]
			},
			userReview: {
				type: String,
				required: [true, "User message is required"]
			}

		}
	]
})

// Making the order schema exportable
module.exports = mongoose.model("Rating", ratingSchema);