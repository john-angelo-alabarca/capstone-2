// Setting up dependencies
const jwt = require('jsonwebtoken');
const secret = "DevelopedByJohn";

// Creating the token
module.exports.createAccessToken = (user) => {
	const userData = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(userData, secret, {});
}

// Verifying the token
module.exports.verify = (req, res, next) => {
	// Getting the created token stored in header.authorization
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		// Removing the word "Bearer " from the random string of the token
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, userData) => {
			if(err){
				return res.send({auth: "failed"});
			}else{
				next();
			}
		});
	}else{
		return res.send({auth: "failed"}); //Does not have token
	}
}

// Decoding the token
module.exports.decode = (token) => {
	// Check if token is present
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, userData) => {
			if(err){
				return null;
			}else{
				// Decoding the whole token by using complete: true
				return jwt.decode(token, {complete: true}).payload
			}
		});
	}else{
		return null; //no token
	}
}

// Checking if the user is an admin
module.exports.admin = (req, res, next) => {
	let token = req.headers.authorization;
		// Check if token is present
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		// Verifying the token if it is an admin
		return jwt.verify(token, secret, (err, userData) => {
			if(err){
				return null;
			}else{
				if(userData.isAdmin === true){
					next();
				}else{
					res.send({auth: "Not authorized"});
				}	
			} 
		});
	}else{
		return null + " No token"; //no token
	}
}

// Checking if it is non admin user
// Checking if the user is an admin
module.exports.nonAdmin = (req, res, next) => {
	let token = req.headers.authorization;
		// Check if token is present
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		// Verifying the token if it is not an admin
		return jwt.verify(token, secret, (err, userData) => {
			if(err){
				return null;
			}else{
				if(userData.isAdmin === false){
					next();
				}else{
					res.send({auth: "Not authorized"});
				}	
			} 
		});
	}else{
		return null + " No token"; //no token
	}
}