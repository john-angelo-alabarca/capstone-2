// Setting up mongoose dependency
const mongoose = require('mongoose');

// Making a order schema (blueprint/model)
const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User Id is required"]
	},
	userName: {
			type: String,
			required: [true, "User's name is required"]
	},
	orderDetails: [
		{
		    customerName: {
				type: String,
				required: [true, "User's name is required"]
			},	
			status: {
				type: String,
				default: "completed"
			},
			productId: {
				type: String,
				required: [true, "Product Id is required"]
			},
			productName: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			},
			paymentMethod : {
				type: String,
				default: ""
			},
			price: {
				type: Number,
				required: [true, "Price is required"]
			},
			ratings: {
				type: Number,
				default: 0
			},
			reviews: {
				type: String,
				default: ""
			},
			subtotal: {
				type: Number,
				required: [true, "Subtotal is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	],
	totalAmount: {
		type: Number,
		default: 0
	}
});

// Making the order schema exportable
module.exports = mongoose.model("Order", orderSchema);