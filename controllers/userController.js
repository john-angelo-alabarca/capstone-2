// Setting up bcrypt and importing auth and model files
const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order');
const Rating = require('../models/rating')
const Passcode = require('../models/passcode');
const auth = require('../auth');
const bcrypt = require('bcrypt');
//test//
// Function for checking email duplication
module.exports.checkDuplicateEmail = (body) => {
	
	return User.findOne({email: body.email}).then(result => {
		if(result){
			return true; // duplicate email exists
		}else{
			return false; //no duplicate email found
		}
	});
}

// Function for checking mobile number duplicate
module.exports.checkMobileNumber = (body) => {
	return User.findOne({mobileNumber: body.mobileNumber}).then(result => {
		if(result){
			return true; //duplicate mobile number
		}else{
			return false; //no duplicate mobile number found
		}
	});
}
// To save passcode only
// module.exports.passcodeSave = (body) => {
// 	let newPasscode = new Passcode({
// 		passcode: bcrypt.hashSync(body.passcode, 10)
// 	});
// 	return newPasscode.save().then((pass, error) => {
// 		if(error){
// 			return false;
// 		}else{
// 			return true;
// 		}
// 	})
// }

// Function for user registration
module.exports.registerUser = (body) => {
	let newUser = new User({
		email: body.email,
		mobileNumber: body.mobileNumber,
		firstName: body.firstName,
		lastName: body.lastName,
		password: bcrypt.hashSync(body.password, 10),
		shippingAddress: [
			{
				province: body.shippingAddress[0].province,
				cityMunicipalityDistrict: body.shippingAddress[0].cityMunicipalityDistrict,
				addressInformation: body.shippingAddress[0].addressInformation
			}	
		] 

	});
	// Saving first the new user information
	return newUser.save().then((user, error) => {
		// console.log(user.isAdmin)
		if(error){
			return false; //user failed to register
		}else{
			// After saving the user's info, fetching the userId and userName and saving it into Order database
			let newOrder = new Order({
				userId: user._id,
				userName: user.firstName + " " + user.lastName,
			});
			// Saving the new order info
			return newOrder.save().then((order, error) => {
				if(error){
					return false; //Failed to save the new order info
				}else{
					return true; //Successfully saved the info	
				}
			});		
		}
	});
}

// Function for user login
module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(!result){
			return false; //user does not exist
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)
				if(isPasswordCorrect){
					return {access: auth.createAccessToken(result.toObject())}
				}else{
					return false; //Password is incorrect
				}
		}
	});
}

// Function for admin login
module.exports.loginAdmin = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(!result){
			return false; //user does not exist
		}else{
			return User.find({isAdmin:true}).then(admin => {
				if(admin.length > 0){
					return false ;
				}else{
					const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)
						if(isPasswordCorrect){
							// Accessing passcode in db
						return Passcode.findOne({isAdmin:{$exists: true}}).then(pass => {
							// Checks if there's a passcode
							if(body.passcode != undefined){
								// Checking if the passcode is correct
								const isPasscodeCorrect = bcrypt.compareSync(body.passcode, pass.passcode)
								if(isPasscodeCorrect){
									let updateAdmin = {
										isAdmin: true
									}
									// Finding the user and updating it into admin
									return User.findByIdAndUpdate(result._id, updateAdmin).then((admin, error) => {
										if(error){
											return false;
										}else{
											return {access: auth.createAccessToken(result.toObject())}
										}
									}) 
								}else{ 
									return "Your passcode is incorrect";
								}
							}else{
								return false;
							}
						});
						}else{
							return false; //Password is incorrect
						}
					
				}
			})
		}
	});
}

// Function for user getting profile 
module.exports.getProfile = (userId) => {
	//finding the user and hiding the id, shipping adddres's id to display
	return User.findOne({_id: userId}).then(result => {
		result.password = undefined; //making the password to hide
		return result;
	});
}

// Function for updating user's profile
module.exports.updateProfile = (userId, body) => {
	// Creating a new updated user info
	let updatedProfile = {
		email: body.email,
		mobileNumber: body.mobileNumber,
		firstName: body.firstName,
		lastName: body.lastName,
	}
	// Finding the user's info and updating it in the db
	return User.findByIdAndUpdate(userId, updatedProfile).then((profile, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	});
}

// Function for updating the user's shipping address
module.exports.updateShippingAddress = (userId, body) => {
	// Creating a new updated shipping address
	let updatedShippingAddress ={
		shippingAddress: [
		      {
		          province: body.shippingAddress[0].province ,
		          cityMunicipalityDistrict: body.shippingAddress[0].cityMunicipalityDistrict,
		          addressInformation: body.shippingAddress[0].addressInformation
		      }   
		  ]
	}
	// Finding the user's info and updating the shipping address in the db
	return User.findByIdAndUpdate(userId, updatedShippingAddress).then((address, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	});
}

// Function for changing user's password
module.exports.changePass = (userId, body) => {
	// Finding the user's info in database
	return User.findOne({_id: userId}).then(result => {
		if(!result){

			return false;
		}else{
			// Checking first if the old password is correct
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)
			if(isPasswordCorrect){
				// Creating a new updated password
				let updatedPassword ={
					password: bcrypt.hashSync(body.newPassword, 10)
				}
				// Finding the user's info and updating the password in the db
				return User.findByIdAndUpdate(userId, updatedPassword).then((pass, error) => {
					if(error){
						return false;
					}else{
						return true;
					}
				});
			}else{
				return false; //Password is incorrect
			}
		

		}
	})
	
}


// Function for deleteing user's own profile (deactivation)
module.exports.deactivate = (params) => {
	// Making the user's account to false
	let deactivateAccount = {
		isActive: false
	}
	// finding and saving in into db
	return User.findByIdAndUpdate(params.userId, deactivateAccount).then((account, error) => {
		if(error){
			return false;
		}else{ 
			return true;
		}
	});
}

// Function to get all the user's profile (Admin only)
module.exports.getAllProfile = () => {
	return User.find({email:{$exists: true}}).then(result => {
		if(result.length === 0){
			return false + " No available users";
		}else{
			return result;
		}
	});
}

// Function to update a user into admin
module.exports.updateToAdmin = (params) => {
	let updatedToAdmin = {
		isAdmin: true
	}
	//Getting the user's info and making it into admin
	return User.findByIdAndUpdate(params.userId, updatedToAdmin).then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	});
}

// Function to update an admin into non-admin user
module.exports.updateToNonAdmin = (params) => {
	let updatedToNonAdmin = {
		isAdmin: false
	}
	//Getting the user's info and making it into admin
	return User.findByIdAndUpdate(params.userId, updatedToNonAdmin).then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	});
}

// Function to buy a new product
module.exports.buy = (orderData) => {
	//Finding the user's orders information and pushing all of the orderDetails that have been made
	 return Order.findOne({userId: orderData.userId}).then(order => {
	 	console.log(order.userName)
		order.orderDetails.push({customerName: order.userName, productId: orderData.productId, productName: orderData.productName, quantity: orderData.quantity,paymentMethod: orderData.paymentMethod, price: orderData.price, subtotal: orderData.subtotal})
		return order.save().then((order, err) => {
			if(err){ 
				return false; // Order is not processed
			}else{	
				return Order.findOne({userId: orderData.userId}).then(order =>{
					let orderArr = order.orderDetails;
					let totalAmt = 0;
					for(let i = 0; i < orderArr.length; i++){
						totalAmt = totalAmt + orderArr[i].subtotal
					}
					let updatedOrder = {
						totalAmount: totalAmt
					}
					return Order.findByIdAndUpdate(order._id, updatedOrder).then((total, error) => {
						if(error){
							return false;
						}else{
							return Product.findById(orderData.productId).then(product => {
								for(let i = 0; i < orderArr.length; i++){
									quantity = orderArr[i].quantity
								}
								
								let remainingStock = (product.stock - quantity)
								let updatedProduct = {
									stock: remainingStock
								}
								return Product.findByIdAndUpdate(product._id, updatedProduct).then((stock, error) => {
									if(error){
										return false
									}else{
										return true;
									}
								})
							});	
						}
					})
				});
			}
		}); 
	});
}

// Function for user getting the orders 
module.exports.getOrder = (userId) => {
	//Finding the user's order info and hiding the every id to be displayed
	return Order.find({userId: userId, orderDetails: {$elemMatch :{status:"completed"}}}).then(result => {
		if(result){
			return result;
		}else{
			return false;  //Not found
		}   
	});
}

// Function for users to rate products
module.exports.rateProduct = (params, body) => {
		return Order.findOneAndUpdate({_id:params.userOrderId, "orderDetails._id": params.productOrderId}, { $set:{"orderDetails.$.ratings": body.ratings, "orderDetails.$.reviews": body.reviews}}).then((rating, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	
}

// Function to get all the user's order
module.exports.getAllOrder = () => {
	// Getting the user's order info with status: completed
	return Order.find({orderDetails: {$elemMatch :{status: "completed"}}}).then(result => {
		if(result.length === 0){
			return false; 
		}else{
			return result;
		}
	});
}



