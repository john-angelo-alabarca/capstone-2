// Setting up mongoose dependency
const mongoose = require('mongoose');

// Making a product schema (blueprint/model)
const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	stock: {
		type: Number,
		required: [true, "Stock is required"]
	},
	category: {
		type: String,
		required: [true, "Category is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	size: {
		type: String,
		required: [true, "Size is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	totalRatings: {
		type: Number,
		default: 0
	},
	isAvailable: {
		type: Boolean,
		default: true
	},
	addedOn: {
		type: Date,
		default: new Date()
	}
});

// Making product schema exportable
module.exports = mongoose.model("Product", productSchema);