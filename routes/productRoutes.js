// Setting up dependencies
const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// Route to get all available/active products
router.get("/getAll", (req, res) => {
	productController.getAll().then(result => res.send(result));
});


// Route to get a specific product
router.get("/:productId", (req, res) => {
	productController.getSpecificProduct(req.params).then(result => res.send(result));
})


// Route to add new product
router.post("/", auth.verify, auth.admin, (req, res) => {
	productController.addProduct(req.body).then(result => res.send(result));
});	


// Route to update a product
router.put("/:productId", auth.verify, auth.admin, (req, res) => {
	productController.updateProduct(req.params, req.body).then(result => res.send(result));
});	

// Route to archive or deleting a product
router.put("/:productId/archive", auth.verify, auth.admin, (req, res) => {
	productController.archiveProduct(req.params, req.body).then(result => res.send(result));
});

// Route to unarchive a product
router.put("/:productId/restore", auth.verify, auth.admin, (req, res) => {
	productController.unArchiveProduct(req.params).then(result => res.send(result));
});

// To make the router exportable
module.exports = router;