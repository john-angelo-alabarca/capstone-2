// Setting up mongoose dependency
const mongoose = require('mongoose');

// Making a user data schema (blueprint)
const userSchema = new mongoose.Schema({
	isAdmin: {
		type: Boolean,
		default: false
	},
	isActive: {
		type: Boolean,
		default: true
	},
	email: {
		type: String,
		required:[true, "Email is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile number is requiredq"]
	},
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	shippingAddress: [
		{
			province: {
				type: String,
				required: [true, "Province is required"]
			},
			cityMunicipalityDistrict: {
				type: String,
				required: [true, "City is required"]
			},
			addressInformation: {
				type: String,
				required: [true, "Address information is required"]
			}
		}
	]
});

// Making the schema exportable
module.exports = mongoose.model("User", userSchema);